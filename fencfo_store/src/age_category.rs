use fencfo_sql_utils::{get_column, get_from_row};
use sea_query::{Alias, Expr, IdenStatic, JoinType, Query, SelectStatement, SqliteQueryBuilder};
use sea_query_binder::SqlxBinder;
use serde::{Deserialize, Serialize};
use sqlx::{sqlite::SqliteRow, SqliteConnection};

#[derive(Serialize, Deserialize, Clone)]
pub struct AgeCategory {
    id: i16,
    code: String,
}

#[derive(IdenStatic, Clone, Copy)]
enum AgeCategories {
    Table,
    Id,
    Code,
}

impl AgeCategory {
    pub async fn get_all(conn: &mut SqliteConnection) -> Result<Vec<Self>, sqlx::Error> {
        let mut stmt = Query::select();
        AgeCategories::get_columns(&mut stmt, Alias::new(AgeCategories::Table.as_str()));
        let (sql, values) = stmt
            .from(AgeCategories::Table)
            .build_sqlx(SqliteQueryBuilder);
        sqlx::query_with(&sql, values)
            .map(|row| Self::from_row(row, None))
            .fetch_all(conn)
            .await
    }

    pub fn extend_select_on_id(
        stmt: &mut SelectStatement,
        join_type: JoinType,
        join_expr: Expr,
        alias: Option<Alias>,
    ) {
        let alias = alias.unwrap_or_else(|| Alias::new(AgeCategories::Table.as_str()));
        stmt.join_as(
            join_type,
            AgeCategories::Table,
            alias.clone(),
            join_expr.equals((alias.clone(), AgeCategories::Id)),
        );
        AgeCategories::get_columns(stmt, alias);
    }

    pub fn from_row(row: SqliteRow, alias: Option<Alias>) -> Self {
        let alias = alias.unwrap_or_else(|| Alias::new(AgeCategories::Table.as_str()));
        Self {
            id: get_from_row(&row, alias.clone(), AgeCategories::Id),
            code: get_from_row(&row, alias, AgeCategories::Code),
        }
    }

    pub fn id(&self) -> i16 {
        self.id
    }
}

impl AgeCategories {
    fn get_columns(stmt: &mut SelectStatement, alias: Alias) {
        get_column(stmt, alias.clone(), Self::Id);
        get_column(stmt, alias, Self::Code);
    }
}
