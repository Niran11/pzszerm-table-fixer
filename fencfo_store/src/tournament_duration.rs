use chrono::NaiveDate;
use serde::Serialize;

#[derive(Serialize)]
pub struct TournamentDuration {
    begin: NaiveDate,
    end: NaiveDate,
}

impl TournamentDuration {
    pub fn new(begin: NaiveDate, end: NaiveDate) -> Self {
        Self { begin, end }
    }

    pub fn begin(&self) -> &NaiveDate {
        &self.begin
    }

    pub fn end(&self) -> &NaiveDate {
        &self.end
    }
}
