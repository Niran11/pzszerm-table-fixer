use serde::Serialize;

#[derive(Serialize)]
pub struct TournamentLocation {
    city_name: String,
    country: Option<TournamentCountry>,
}

#[derive(Serialize)]
pub struct TournamentCountry {
    name: String,
    code: String,
}

impl TournamentLocation {
    pub fn new_without_country(city_name: String) -> Self {
        Self {
            city_name,
            country: None,
        }
    }

    pub fn city_name(&self) -> &str {
        &self.city_name
    }
}
