use serde::Serialize;

#[derive(Serialize)]
pub struct TournamentName {
    name: String,
    url: String,
}

impl TournamentName {
    pub fn new(name: String, url: String) -> Self {
        Self { name, url }
    }

    pub fn url(&self) -> &str {
        &self.url
    }

    pub fn name(&self) -> &str {
        &self.name
    }
}
