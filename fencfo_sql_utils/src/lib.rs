use sea_query::{Alias, Expr, Iden, IdenStatic, SelectStatement};
use sqlx::{sqlite::SqliteRow, Row};

pub fn get_column<T: IdenStatic + Copy>(stmt: &mut SelectStatement, alias: Alias, column: T) {
    stmt.expr_as(
        Expr::col((alias.clone(), column)),
        Alias::new(&get_returned_column_name(alias, column)),
    );
}

pub fn get_from_row<'r, T, U>(row: &'r SqliteRow, alias: Alias, column: T) -> U
where
    T: IdenStatic,
    U: sqlx::Decode<'r, <SqliteRow as Row>::Database> + sqlx::Type<<SqliteRow as Row>::Database>,
{
    row.get(get_returned_column_name(alias, column).as_str())
}

pub fn get_returned_column_name<T: IdenStatic>(alias: Alias, column: T) -> String {
    format!("{}_{}", alias.to_string(), column.as_str())
}
