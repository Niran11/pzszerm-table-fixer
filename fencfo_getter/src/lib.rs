use std::time::Duration;

use chrono::{DateTime, Duration as ChronoDuration, TimeZone, Utc};
use fencfo_pzszerm_getter::update_pzszerm_tournaments;
use sqlx::SqlitePool;

mod error;
use error::Error;

pub fn create_getter_loop(db_pool: SqlitePool) {
    tokio::spawn(background_thread(db_pool));
}

async fn background_thread(db_pool: SqlitePool) {
    loop {
        let now = Utc::now();
        if let Err(e) = update_tournaments(&db_pool, &now).await {
            eprintln!("{}", e);
            continue;
        }
        let next_day = now + ChronoDuration::days(1);
        let next_midnight = if let Some(date) = next_day.date_naive().and_hms_opt(0, 0, 1) {
            Utc.from_utc_datetime(&date)
        } else {
            continue;
        };
        let next_run = (next_midnight - now)
            .to_std()
            .unwrap_or(Duration::from_secs(300));
        tokio::time::sleep(next_run).await;
    }
}

async fn update_tournaments(db_pool: &SqlitePool, now: &DateTime<Utc>) -> Result<(), Error> {
    let mut conn = db_pool.acquire().await?;
    update_pzszerm_tournaments(&mut conn, now).await?;
    Ok(())
}
