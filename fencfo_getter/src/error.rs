use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Could not communicate with db: {0}")]
    Sqlx(sqlx::Error),

    #[error("Could not update pzszerm table: {0}")]
    Pzszerm(fencfo_pzszerm_getter::Error),
}

impl From<sqlx::Error> for Error {
    fn from(value: sqlx::Error) -> Self {
        Self::Sqlx(value)
    }
}

impl From<fencfo_pzszerm_getter::Error> for Error {
    fn from(value: fencfo_pzszerm_getter::Error) -> Self {
        Self::Pzszerm(value)
    }
}
