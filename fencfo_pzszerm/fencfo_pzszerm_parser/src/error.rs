use scraper::error::SelectorErrorKind as ScraperError;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Scraper gave up, so do I")]
    ScraperFailed,

    #[error("Could not find tournaments table in provided page")]
    TableNotFound,

    #[error("Could not parse tournament row - cannot find url")]
    NoTournamentUrl,

    #[error("Could not parse tournament row - cannot find name")]
    NoTournamentName,

    #[error("Could not parse tournament row - cannot find categories")]
    NoCategoryCode,

    #[error("Could not parse tournament row - cannot find weapons")]
    NoWeaponCode,

    #[error("Could not parse tournament row - cannot find season")]
    NoSeason,

    #[error("Could not parse tournament row - cannot find duration beginning")]
    NoDurationBegin,

    #[error("Could not parse tournament row - cannot find duration ending")]
    NoDurationEnd,

    #[error("Could not parse tournament row - cannot find location")]
    NoLocation,

    #[error("{0}")]
    PzszermTournamentNoId(fencfo_pzszerm_store::Error),
}

impl From<ScraperError<'_>> for Error {
    fn from(_: ScraperError) -> Self {
        Self::ScraperFailed
    }
}

impl From<fencfo_pzszerm_store::Error> for Error {
    fn from(value: fencfo_pzszerm_store::Error) -> Self {
        Self::PzszermTournamentNoId(value)
    }
}
