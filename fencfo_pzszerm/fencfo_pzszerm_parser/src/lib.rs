use chrono::NaiveDate;
use scraper::{ElementRef, Html, Selector};

mod error;
mod parsed_tournament;

pub use error::Error;
pub use parsed_tournament::ParsedPzszermTournament;

pub async fn parse_pzszerm_tournaments_page(
    page_text: String,
    now: &NaiveDate,
) -> Result<(Vec<ParsedPzszermTournament>, bool), Error> {
    let page = Html::parse_document(&page_text);
    let page_table = get_table(&page)?;
    parse_table(page_table, now)
}

fn get_table(page: &Html) -> Result<ElementRef, Error> {
    let table_selector = Selector::parse("table")?;
    page.select(&table_selector)
        .next()
        .ok_or(Error::TableNotFound)
}

fn parse_table(
    table: ElementRef,
    now: &NaiveDate,
) -> Result<(Vec<ParsedPzszermTournament>, bool), Error> {
    let tr_selector = Selector::parse("tr")?;
    let table_trs = table.select(&tr_selector);
    let mut tournaments = Vec::new();
    let mut only_future = true;
    for tr in table_trs {
        if !is_date(&tr)? && !is_head(&tr)? {
            let tournament = parse_tournament(&tr)?;
            if !tournament.is_in_future(now) {
                only_future = false;
                break;
            }
            tournaments.push(tournament);
        }
    }
    Ok((tournaments, only_future))
}

fn is_date(tr: &ElementRef) -> Result<bool, Error> {
    let date_selector = Selector::parse("td[colspan='5'] strong")?;
    let found_date = tr.select(&date_selector).next().is_some();
    Ok(found_date)
}

fn is_head(tr: &ElementRef) -> Result<bool, Error> {
    let head_selector = Selector::parse("th.tab-top strong")?;
    let found_head = tr.select(&head_selector).next().is_some();
    Ok(found_head)
}

fn parse_tournament(tr: &ElementRef) -> Result<ParsedPzszermTournament, Error> {
    let td_selector = Selector::parse("td.tab-row")?;
    let tr_tds = tr.select(&td_selector);
    ParsedPzszermTournament::parse_from_tds(tr_tds)
}
