use chrono::NaiveDate;
use fencfo_pzszerm_store::{
    PzszermAgeCategoryMapper, PzszermTournament, PzszermWeaponMapper, Season, TournamentDuration,
    TournamentLocation, TournamentName,
};
use scraper::{element_ref::Select, ElementRef, Selector};

use crate::Error;

pub struct ParsedPzszermTournament {
    name: TournamentName,
    age_categories: Vec<String>,
    weapons: Vec<String>,
    season: Season,
    duration: TournamentDuration,
    location: TournamentLocation,
}

impl ParsedPzszermTournament {
    pub(super) fn parse_from_tds(mut tr_tds: Select) -> Result<Self, Error> {
        let me = Self {
            name: Self::get_tournament_name(tr_tds.next().ok_or(Error::NoTournamentName)?)?,
            age_categories: Self::get_age_categories(tr_tds.next().ok_or(Error::NoCategoryCode)?)?,
            weapons: Self::get_weapons(tr_tds.next().ok_or(Error::NoWeaponCode)?)?,
            season: Self::get_season(tr_tds.next().ok_or(Error::NoSeason)?)?,
            duration: Self::get_duration(
                tr_tds.next().ok_or(Error::NoDurationBegin)?,
                tr_tds.next().ok_or(Error::NoDurationEnd)?,
            )?,
            location: Self::get_location(tr_tds.next().ok_or(Error::NoLocation)?)?,
        };
        Ok(me)
    }

    fn get_tournament_name(name_td: ElementRef) -> Result<TournamentName, Error> {
        let inner_url = name_td
            .select(&Selector::parse("a")?)
            .next()
            .ok_or(Error::NoTournamentUrl)?;
        let name = inner_url.text().next().ok_or(Error::NoTournamentName)?;
        let url = inner_url
            .value()
            .attr("href")
            .ok_or(Error::NoTournamentUrl)?;
        Ok(TournamentName::new(name.to_owned(), url.to_owned()))
    }

    fn get_age_categories(categories_td: ElementRef) -> Result<Vec<String>, Error> {
        let mut categories = Vec::new();
        for category in categories_td.inner_html().split(',') {
            let code_begin = category.rfind('(').ok_or(Error::NoCategoryCode)? + 1;
            let code_end = category.rfind(')').ok_or(Error::NoCategoryCode)?;
            let code = category
                .get(code_begin..code_end)
                .ok_or(Error::NoCategoryCode)?;
            categories.push(code.to_uppercase())
        }
        Ok(categories)
    }

    fn get_weapons(weapons_td: ElementRef) -> Result<Vec<String>, Error> {
        let mut weapons = Vec::new();
        for weapon in weapons_td.inner_html().split(',') {
            let code_begin = weapon.rfind('(').ok_or(Error::NoWeaponCode)? + 1;
            let code_end = weapon.rfind(')').ok_or(Error::NoWeaponCode)?;
            let code = weapon
                .get(code_begin..code_end)
                .ok_or(Error::NoWeaponCode)?;
            weapons.push(code.to_uppercase())
        }
        Ok(weapons)
    }

    fn get_season(season_td: ElementRef) -> Result<Season, Error> {
        let (season_begin, season_end) = season_td
            .inner_html()
            .split_once('/')
            .and_then(|(b, e)| Some((b.parse().ok()?, e.parse().ok()?)))
            .ok_or(Error::NoSeason)?;
        Ok(Season::new(season_begin, season_end))
    }

    fn get_duration(
        duration_begin_td: ElementRef,
        duration_end_td: ElementRef,
    ) -> Result<TournamentDuration, Error> {
        let duration_begin = NaiveDate::parse_from_str(&duration_begin_td.inner_html(), "%d.%m.%Y")
            .map_err(|_| Error::NoDurationBegin)?;
        let duration_end = NaiveDate::parse_from_str(&duration_end_td.inner_html(), "%d.%m.%Y")
            .map_err(|_| Error::NoDurationEnd)?;
        Ok(TournamentDuration::new(duration_begin, duration_end))
    }

    fn get_location(location_td: ElementRef) -> Result<TournamentLocation, Error> {
        let location_name = location_td.inner_html();
        Ok(TournamentLocation::new_without_country(location_name))
    }

    pub(super) fn is_in_future(&self, now: &NaiveDate) -> bool {
        *self.duration.end() >= *now
    }

    pub fn to_pzszerm(
        self,
        age_categories: &PzszermAgeCategoryMapper,
        weapons: &PzszermWeaponMapper,
    ) -> Result<PzszermTournament, Error> {
        let tournament = PzszermTournament::new(
            self.name,
            self.age_categories
                .iter()
                .filter_map(|c| age_categories.get(c))
                .collect(),
            self.weapons.iter().filter_map(|c| weapons.get(c)).collect(),
            self.season,
            self.duration,
            self.location,
        );
        Ok(tournament?)
    }
}
