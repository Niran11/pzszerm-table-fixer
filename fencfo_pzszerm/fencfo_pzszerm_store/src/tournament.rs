use fencfo_sql_utils::{get_column, get_from_row};
use fencfo_store::{AgeCategory, TournamentDuration, TournamentLocation, TournamentName, Weapon};
use sea_query::{
    Alias, Expr, IdenStatic, JoinType, Order, Query, SelectStatement, SqliteQueryBuilder,
};
use sea_query_binder::SqlxBinder;
use serde::Serialize;
use sqlx::{sqlite::SqliteRow, SqliteConnection};

use crate::{Error, Season};

#[derive(Serialize)]
pub struct PzszermTournament {
    id: i64,
    name: TournamentName,
    age_categories: Vec<AgeCategory>,
    weapons: Vec<Weapon>,
    season: Season,
    duration: TournamentDuration,
    location: TournamentLocation,
}

#[derive(IdenStatic, Clone, Copy)]
enum PzszermTournaments {
    Table,
    Id,
    Name,
    Url,
    SeasonBegin,
    SeasonEnd,
    DurationBegin,
    DurationEnd,
    CityName,
}

#[derive(IdenStatic, Clone, Copy)]
enum PzszermTournamentsAgeCategories {
    Table,
    TournamentId,
    CategoryId,
}

#[derive(IdenStatic, Clone, Copy)]
enum PzszermTournamentsWeapons {
    Table,
    TournamentId,
    WeaponId,
}

impl PzszermTournament {
    pub async fn get_ordered(conn: &mut SqliteConnection) -> Result<Vec<Self>, sqlx::Error> {
        let mut stmt = Query::select();
        stmt.from(PzszermTournaments::Table);
        PzszermTournaments::extend_select(&mut stmt);
        stmt.order_by(PzszermTournaments::DurationEnd, Order::Asc);
        let (sql, values) = stmt.build_sqlx(SqliteQueryBuilder);
        let mut tournaments = sqlx::query_with(&sql, values)
            .map(|row| Self::from_row(row, None))
            .fetch_all(&mut *conn)
            .await?;
        for tournament in tournaments.iter_mut() {
            tournament.age_categories =
                PzszermTournamentsAgeCategories::get_in_tournament(conn, tournament.id).await?;
            tournament.weapons =
                PzszermTournamentsWeapons::get_in_tournament(conn, tournament.id).await?;
        }
        Ok(tournaments)
    }

    fn from_row(row: SqliteRow, alias: Option<Alias>) -> Self {
        let alias = alias.unwrap_or_else(|| Alias::new(PzszermTournaments::Table.as_str()));
        Self {
            id: get_from_row(&row, alias.clone(), PzszermTournaments::Id),
            name: TournamentName::new(
                get_from_row(&row, alias.clone(), PzszermTournaments::Name),
                get_from_row(&row, alias.clone(), PzszermTournaments::Url),
            ),
            age_categories: Vec::with_capacity(0),
            weapons: Vec::with_capacity(0),
            season: Season::new(
                get_from_row(&row, alias.clone(), PzszermTournaments::SeasonBegin),
                get_from_row(&row, alias.clone(), PzszermTournaments::SeasonEnd),
            ),
            duration: TournamentDuration::new(
                get_from_row(&row, alias.clone(), PzszermTournaments::DurationBegin),
                get_from_row(&row, alias.clone(), PzszermTournaments::DurationEnd),
            ),
            location: TournamentLocation::new_without_country(get_from_row(
                &row,
                alias,
                PzszermTournaments::CityName,
            )),
        }
    }

    pub fn new(
        name: TournamentName,
        age_categories: Vec<AgeCategory>,
        weapons: Vec<Weapon>,
        season: Season,
        duration: TournamentDuration,
        location: TournamentLocation,
    ) -> Result<Self, Error> {
        let id = name
            .url()
            .split_once("id=")
            .and_then(|(_, u)| u.parse().ok())
            .ok_or(Error::NoId)?;
        let me = Self {
            id,
            name,
            age_categories,
            weapons,
            season,
            duration,
            location,
        };
        Ok(me)
    }

    pub async fn store_vec(
        conn: &mut SqliteConnection,
        tournaments: Vec<Self>,
    ) -> Result<(), sqlx::Error> {
        if tournaments.is_empty() {
            Ok(())
        } else {
            Self::remove(conn).await?;
            Self::insert(conn, &tournaments).await?;
            PzszermTournamentsAgeCategories::insert(
                conn,
                tournaments.iter().map(|t| (t.id, &t.age_categories)),
            )
            .await?;
            PzszermTournamentsWeapons::insert(conn, tournaments.iter().map(|t| (t.id, &t.weapons)))
                .await
        }
    }

    async fn remove(conn: &mut SqliteConnection) -> Result<(), sqlx::Error> {
        let (sql, values) = Query::delete()
            .from_table(PzszermTournaments::Table)
            .build_sqlx(SqliteQueryBuilder);
        sqlx::query_with(&sql, values).execute(conn).await?;
        Ok(())
    }

    async fn insert(conn: &mut SqliteConnection, tournaments: &[Self]) -> Result<(), sqlx::Error> {
        let mut stmt = Query::insert();
        stmt.into_table(PzszermTournaments::Table).columns([
            PzszermTournaments::Id,
            PzszermTournaments::Name,
            PzszermTournaments::Url,
            PzszermTournaments::SeasonBegin,
            PzszermTournaments::SeasonEnd,
            PzszermTournaments::DurationBegin,
            PzszermTournaments::DurationEnd,
            PzszermTournaments::CityName,
        ]);
        for tournament in tournaments {
            stmt.values([
                tournament.id.into(),
                tournament.name.name().into(),
                tournament.name.url().into(),
                tournament.season.begin().into(),
                tournament.season.end().into(),
                (*tournament.duration.begin()).into(),
                (*tournament.duration.end()).into(),
                tournament.location.city_name().into(),
            ])
            .map_err(|_| sqlx::Error::WorkerCrashed)?;
        }
        let (sql, values) = stmt.build_sqlx(SqliteQueryBuilder);
        sqlx::query_with(&sql, values).execute(conn).await?;
        Ok(())
    }
}

impl PzszermTournaments {
    fn extend_select(stmt: &mut SelectStatement) {
        let alias = Alias::new(Self::Table.as_str());
        get_column(stmt, alias.clone(), Self::Id);
        get_column(stmt, alias.clone(), Self::Name);
        get_column(stmt, alias.clone(), Self::Url);
        get_column(stmt, alias.clone(), Self::SeasonBegin);
        get_column(stmt, alias.clone(), Self::SeasonEnd);
        get_column(stmt, alias.clone(), Self::DurationBegin);
        get_column(stmt, alias.clone(), Self::DurationEnd);
        get_column(stmt, alias, Self::CityName);
    }
}

impl PzszermTournamentsAgeCategories {
    async fn insert<'a, T: Iterator<Item = (i64, &'a Vec<AgeCategory>)>>(
        conn: &mut SqliteConnection,
        tournaments: T,
    ) -> Result<(), sqlx::Error> {
        let mut stmt = Query::insert();
        stmt.into_table(PzszermTournamentsAgeCategories::Table)
            .columns([
                PzszermTournamentsAgeCategories::TournamentId,
                PzszermTournamentsAgeCategories::CategoryId,
            ]);
        for (t_id, categories) in tournaments {
            for category in categories {
                stmt.values([t_id.into(), category.id().into()])
                    .map_err(|_| sqlx::Error::WorkerCrashed)?;
            }
        }
        let (sql, values) = stmt.build_sqlx(SqliteQueryBuilder);
        sqlx::query_with(&sql, values).execute(conn).await?;
        Ok(())
    }

    async fn get_in_tournament(
        conn: &mut SqliteConnection,
        tournament_id: i64,
    ) -> Result<Vec<AgeCategory>, sqlx::Error> {
        let mut stmt = Query::select();
        stmt.from(PzszermTournamentsAgeCategories::Table)
            .and_where(Expr::col((Self::Table, Self::TournamentId)).eq(tournament_id));
        AgeCategory::extend_select_on_id(
            &mut stmt,
            JoinType::Join,
            Expr::col((Self::Table, Self::CategoryId)),
            None,
        );
        let (sql, values) = stmt.build_sqlx(SqliteQueryBuilder);
        sqlx::query_with(&sql, values)
            .map(|row| AgeCategory::from_row(row, None))
            .fetch_all(conn)
            .await
    }
}

impl PzszermTournamentsWeapons {
    async fn insert<'a, T: Iterator<Item = (i64, &'a Vec<Weapon>)>>(
        conn: &mut SqliteConnection,
        tournaments: T,
    ) -> Result<(), sqlx::Error> {
        let mut stmt = Query::insert();
        stmt.into_table(PzszermTournamentsWeapons::Table).columns([
            PzszermTournamentsWeapons::TournamentId,
            PzszermTournamentsWeapons::WeaponId,
        ]);
        for (t_id, weapons) in tournaments {
            for weapon in weapons {
                stmt.values([t_id.into(), weapon.id().into()])
                    .map_err(|_| sqlx::Error::WorkerCrashed)?;
            }
        }
        let (sql, values) = stmt.build_sqlx(SqliteQueryBuilder);
        sqlx::query_with(&sql, values).execute(conn).await?;
        Ok(())
    }

    async fn get_in_tournament(
        conn: &mut SqliteConnection,
        tournament_id: i64,
    ) -> Result<Vec<Weapon>, sqlx::Error> {
        let mut stmt = Query::select();
        stmt.from(PzszermTournamentsWeapons::Table)
            .and_where(Expr::col((Self::Table, Self::TournamentId)).eq(tournament_id));
        Weapon::extend_select_on_id(
            &mut stmt,
            JoinType::Join,
            Expr::col((Self::Table, Self::WeaponId)),
            None,
        );
        let (sql, values) = stmt.build_sqlx(SqliteQueryBuilder);
        sqlx::query_with(&sql, values)
            .map(|row| Weapon::from_row(row, None))
            .fetch_all(conn)
            .await
    }
}
