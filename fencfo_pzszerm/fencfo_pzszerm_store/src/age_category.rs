use std::collections::HashMap;

use fencfo_sql_utils::{get_column, get_from_row};
use fencfo_store::AgeCategory;
use sea_query::{Alias, Expr, IdenStatic, JoinType, Query, SqliteQueryBuilder};
use sea_query_binder::SqlxBinder;
use sqlx::{sqlite::SqliteRow, SqliteConnection};

pub struct PzszermAgeCategoryMapper {
    mappings: HashMap<String, AgeCategory>,
}

#[derive(IdenStatic, Clone, Copy)]
enum PzszermAgeCategoriesMapper {
    Table,
    PzszermCode,
    CategoryId,
}

impl PzszermAgeCategoryMapper {
    pub async fn new(conn: &mut SqliteConnection) -> Result<Self, sqlx::Error> {
        let mut stmt = Query::select();
        stmt.from(PzszermAgeCategoriesMapper::Table);
        AgeCategory::extend_select_on_id(
            &mut stmt,
            JoinType::Join,
            Expr::col((
                PzszermAgeCategoriesMapper::Table,
                PzszermAgeCategoriesMapper::CategoryId,
            )),
            None,
        );
        get_column(
            &mut stmt,
            Alias::new(PzszermAgeCategoriesMapper::Table.as_str()),
            PzszermAgeCategoriesMapper::PzszermCode,
        );
        let (sql, values) = stmt.build_sqlx(SqliteQueryBuilder);
        let rows = sqlx::query_with(&sql, values)
            .map(|row| Self::from_row(row, None))
            .fetch_all(conn)
            .await?;
        Ok(Self {
            mappings: HashMap::from_iter(rows),
        })
    }

    fn from_row(row: SqliteRow, alias: Option<Alias>) -> (String, AgeCategory) {
        let alias = alias.unwrap_or_else(|| Alias::new(PzszermAgeCategoriesMapper::Table.as_str()));
        (
            get_from_row(&row, alias, PzszermAgeCategoriesMapper::PzszermCode),
            AgeCategory::from_row(row, None),
        )
    }

    pub fn get(&self, key: &str) -> Option<AgeCategory> {
        self.mappings.get(key).cloned()
    }
}
