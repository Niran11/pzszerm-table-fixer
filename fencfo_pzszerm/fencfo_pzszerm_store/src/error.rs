use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Given tournament url lacks id!")]
    NoId,
}
