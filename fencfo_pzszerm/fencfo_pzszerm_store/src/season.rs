use serde::Serialize;

#[derive(Serialize)]
pub struct Season {
    begin: i16,
    end: i16,
}

impl Season {
    pub fn new(begin: i16, end: i16) -> Self {
        Self { begin, end }
    }

    pub fn begin(&self) -> i16 {
        self.begin
    }

    pub fn end(&self) -> i16 {
        self.end
    }
}
