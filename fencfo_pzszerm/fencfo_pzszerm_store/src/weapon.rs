use std::collections::HashMap;

use fencfo_sql_utils::{get_column, get_from_row};
use fencfo_store::Weapon;
use sea_query::{Alias, Expr, IdenStatic, JoinType, Query, SqliteQueryBuilder};
use sea_query_binder::SqlxBinder;
use sqlx::{sqlite::SqliteRow, SqliteConnection};

pub struct PzszermWeaponMapper {
    mappings: HashMap<String, Weapon>,
}

#[derive(IdenStatic, Clone, Copy)]
enum PzszermWeaponsMapper {
    Table,
    PzszermCode,
    WeaponId,
}

impl PzszermWeaponMapper {
    pub async fn new(conn: &mut SqliteConnection) -> Result<Self, sqlx::Error> {
        let mut stmt = Query::select();
        stmt.from(PzszermWeaponsMapper::Table);
        Weapon::extend_select_on_id(
            &mut stmt,
            JoinType::Join,
            Expr::col((PzszermWeaponsMapper::Table, PzszermWeaponsMapper::WeaponId)),
            None,
        );
        get_column(
            &mut stmt,
            Alias::new(PzszermWeaponsMapper::Table.as_str()),
            PzszermWeaponsMapper::PzszermCode,
        );
        let (sql, values) = stmt.build_sqlx(SqliteQueryBuilder);
        let rows = sqlx::query_with(&sql, values)
            .map(|row| Self::from_row(row, None))
            .fetch_all(conn)
            .await?;
        Ok(Self {
            mappings: HashMap::from_iter(rows),
        })
    }

    fn from_row(row: SqliteRow, alias: Option<Alias>) -> (String, Weapon) {
        let alias = alias.unwrap_or_else(|| Alias::new(PzszermWeaponsMapper::Table.as_str()));
        (
            get_from_row(&row, alias, PzszermWeaponsMapper::PzszermCode),
            Weapon::from_row(row, None),
        )
    }

    pub fn get(&self, key: &str) -> Option<Weapon> {
        self.mappings.get(key).cloned()
    }
}
