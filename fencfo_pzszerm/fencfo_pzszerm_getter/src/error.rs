use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Could not fetch pzszerm page: {0}")]
    Reqwest(reqwest::Error),

    #[error("Could not parse pzszerm page: {0}")]
    Parser(fencfo_pzszerm_parser::Error),

    #[error("Could not load/store pzszerm data in db: {0}")]
    Store(sqlx::Error),
}

impl From<reqwest::Error> for Error {
    fn from(value: reqwest::Error) -> Self {
        Self::Reqwest(value)
    }
}

impl From<fencfo_pzszerm_parser::Error> for Error {
    fn from(value: fencfo_pzszerm_parser::Error) -> Self {
        Self::Parser(value)
    }
}

impl From<sqlx::Error> for Error {
    fn from(value: sqlx::Error) -> Self {
        Self::Store(value)
    }
}
