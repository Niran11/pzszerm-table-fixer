use std::time::Duration;

use chrono::{DateTime, Utc};
use fencfo_pzszerm_parser::{parse_pzszerm_tournaments_page, ParsedPzszermTournament};
use fencfo_pzszerm_store::{PzszermAgeCategoryMapper, PzszermTournament, PzszermWeaponMapper};

mod error;

pub use error::Error;
use sqlx::SqliteConnection;

pub async fn update_pzszerm_tournaments(
    conn: &mut SqliteConnection,
    now: &DateTime<Utc>,
) -> Result<(), Error> {
    let mut only_future = true;
    let mut page = 0;
    let now_date = now.naive_utc().date();
    let mut tournaments = Vec::new();
    let age_categories = PzszermAgeCategoryMapper::new(conn).await?;
    let weapons = PzszermWeaponMapper::new(conn).await?;
    while only_future {
        page += 1;
        let page_text = fetch_page(page).await?;
        let (parsed_tournaments, only_fut) =
            parse_pzszerm_tournaments_page(page_text, &now_date).await?;
        only_future = only_fut;
        let mut normalized =
            normalize_tournaments(&age_categories, &weapons, parsed_tournaments).await?;
        tournaments.append(&mut normalized);
        tokio::time::sleep(Duration::from_millis(250)).await;
    }
    PzszermTournament::store_vec(conn, tournaments).await?;
    Ok(())
}

async fn fetch_page(page_number: i16) -> Result<String, reqwest::Error> {
    reqwest::get(format!(
        "https://pzszerm.pl/zawody/kalendarium-zawodow/?pg={}",
        page_number
    ))
    .await?
    .text()
    .await
}

async fn normalize_tournaments(
    age_categories: &PzszermAgeCategoryMapper,
    weapons: &PzszermWeaponMapper,
    tournaments: Vec<ParsedPzszermTournament>,
) -> Result<Vec<PzszermTournament>, Error> {
    let tournaments = tournaments
        .into_iter()
        .flat_map(|t| {
            let tournament = t.to_pzszerm(age_categories, weapons);
            if let Err(e) = &tournament {
                eprintln!("error when creating pzszerm tournament: {}", e);
            }
            tournament
        })
        .collect();
    Ok(tournaments)
}
