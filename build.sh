#!/bin/bash

if [[ ! -e ./node_modules/tailwindcss ]]; then
  npm install tailwindcss
fi

npx tailwindcss -i "./src/styles.css" -o "./static/styles.css" --minify

"$HOME"/.cargo/bin/cargo watch -w src/ -x 'build --release' -s 'touch .triggerrun'
