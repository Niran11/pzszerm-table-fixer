var settings = {}
var allTables = []

window.onload = () => {
  handleTheme()
  getTables()
}

function handleTheme() {
  changeTheme();
  document.getElementById('theme').addEventListener('click', () => {
    if (localStorage.theme === 'dark') {
      localStorage.theme = 'light'
    } else {
      localStorage.theme = 'dark'
    }
    changeTheme()
  })
}

function changeTheme() {
  if (localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
    document.documentElement.classList.add('dark')
  } else {
    document.documentElement.classList.remove('dark')
  }
}

function getTables() {
  fetch('/pzszerm/get')
    .then(response => response.json())
    .then(tournaments => loadedTournaments(tournaments))
    .catch(() => badMessage('Nie udało się załadować zawodów... Spróbuj odświeżyć?'))
}

function loadedTournaments(tournaments) {
  showTournaments(tournaments)
  document.getElementById('loading').remove()
  document.getElementById('filter-form').addEventListener('submit', ev => filterTournaments(ev, tournaments))
}

function badMessage(text) {
  displayMessages([{ kind: 'bad', 'text': text }])
}

function displayMessages(messages) {
  let container = document.getElementById('messages')
  for (const msg of messages) {
    let element = document.createElement('div');
    element.classList.add('notification', msg.kind)
    element.innerHTML = msg.text
    element.addEventListener('click', function() { this.remove() })
    container.append(element)
  }
}

function filterTournaments(ev, tournaments) {
  ev.preventDefault()
  let new_tournaments = tournaments
    .filter(t => hasWeapon(t.weapons))
    .filter(t => hasAgeCategory(t.age_categories))
  showTournaments(new_tournaments)
}

function hasWeapon(weapons) {
  let selected = document.getElementById('weapons-filter').value;
  return !selected || weapons.findIndex(w => w.code === selected) !== -1
}

function hasAgeCategory(categories) {
  let selected = document.getElementById('age-categories-filter').value;
  return !selected || categories.findIndex(c => c.code === selected) !== -1
}

function showTournaments(tournaments) {
  let container = document.getElementById('tournaments')
  container.innerHTML = ''
  let last_date
  for (const t of tournaments) {
    let date = getDate(t.duration.begin)
    if (!date.isSame(last_date)) {
      last_date = date
      last_date.insert(container)
    }
    let tournament = document.createElement('div')
    tournament.classList.add('tournament')
    let tournament_url = document.createElement('div')
    tournament_url.classList.add('tournament-url')
    let tournament_url_a = document.createElement('a')
    tournament_url_a.href = 'https://pzszerm.pl' + t.name.url
    tournament_url_a.innerHTML = t.name.name
    tournament_url_a.setAttribute('target', '_blank')
    tournament_url.append(tournament_url_a)
    tournament.append(tournament_url)
    let tournament_info_container = document.createElement('div')
    tournament_info_container.classList.add('tournament-info-container')
    insertField(tournament_info_container, getAgeCategoriesAbbr(), getCodedValues(t.age_categories))
    insertField(tournament_info_container, 'Bronie', getCodedValues(t.weapons))
    insertField(tournament_info_container, 'Sezon', t.season.begin + '/' + t.season.end)
    insertField(tournament_info_container, 'Czas trwania', processTournamentTime(t.duration.begin, t.duration.end))
    insertField(tournament_info_container, 'Miejsce', t.location.city_name)
    tournament.append(tournament_info_container)
    container.append(tournament)
  }
}

function getDate(date) {
  let [year, month, _] = date.split('-')
  return {
    'month': month,
    'year': year,
    isSame(other) {
      return other !== undefined && this.month == other.month && this.year == other.year
    },
    insert(container) {
      let el = document.createElement('div')
      el.classList.add('text-center', 'text-2xl', 'p-1', 'my-1')
      let el_strong = document.createElement('strong')
      el_strong.innerHTML = mapMonth(this.month) + ' - ' + this.year
      el.append(el_strong)
      container.append(el)
    }
  }
}

function mapMonth(month) {
  return months[parseInt(month)]
}

const months = {
  1: 'Styczeń',
  2: 'Luty',
  3: 'Marzec',
  4: 'Kwiecień',
  5: 'Maj',
  6: 'Czerwiec',
  7: 'Lipiec',
  8: 'Sierpień',
  9: 'Wrzesień',
  10: 'Październik',
  11: 'Listopad',
  12: 'Grudzień',
}

function getAgeCategoriesAbbr() {
  let abbr = document.createElement('abbr')
  abbr.title = 'Kategorie wiekowe'
  abbr.innerHTML = 'Kat. wiekowe'
  return abbr
}

function getCodedValues(values) {
  let container = []
  values.forEach((el, i) => {
    let [code, name] = coded_values[el.code]
    let el_full = document.createElement('abbr')
    el_full.classList.add('hidden', 'lg:inline')
    el_full.innerHTML = name
    el_full.title = code
    let el_mobile = document.createElement('abbr')
    el_mobile.classList.add('inline', 'lg:hidden')
    el_mobile.innerHTML = code
    el_mobile.title = name
    container.push(el_full, el_mobile)
    if (i < (values.length - 1)) {
      container.push(", ")
    }
  })
  return container
}

const coded_values = {
  'A6U8': ['SK', 'Skrzaty'],
  'U10': ['Z', 'Zuchy'],
  'U12': ['D', 'Dzieci'],
  'U14': ['Mł', 'Młodzicy'],
  'U17': ['Jmł', 'Juniorzy młodsi (kadeci)'],
  'U20': ['J', 'Juniorzy'],
  'U23': ['M', 'Młodzieżowcy'],
  'A24': ['S', 'Seniorzy'],
  'V': ['W', 'Weterani'],
  'EPEE': ['SZP', 'Szpada'],
  'FOIL': ['FL', 'Floret'],
  'SABRE': ['SZB', 'Szabla'],
}

function insertField(conatiner, title, text) {
  let info = document.createElement('div')
  info.classList.add('tournament-info')
  conatiner.append(info)
  let info_box = document.createElement('div')
  info_box.classList.add('tournament-info-box')
  info.append(info_box)
  let div1 = document.createElement('div')
  let div2 = document.createElement('div')
  let title_el = document.createElement('strong')
  title_el.append(title)
  title_el.append(':')
  div2.append(title_el)
  div1.append(div2)
  div1.append(...text)
  info_box.append(div1)
}

function processTournamentTime(start_time, end_time) {
  let [start_year, start_month, start_day] = start_time.split("-")
  let [end_year, end_month, end_day] = end_time.split("-")
  if (start_year == end_year) {
    if (start_month == end_month) {
      if (start_day == end_day) {
        return start_day + '.' + start_month + '.' + start_year
      } else {
        return start_day + ' - ' + end_day + '.' + start_month + '.' + start_year
      }
    } else {
      return start_day + '.' + start_month + ' - ' + end_day + '.' + end_month + '.' + start_year
    }
  } else {
    return start_time + ' - ' + end_time
  }
}
