use actix_web::{post, web, HttpResponse};
use cached::proc_macro::cached;
use scraper::{Html, Selector};
use serde::Deserialize;

use crate::error::Error;

#[derive(Deserialize, Clone, PartialEq, Eq, Hash)]
pub struct Settings {
    page: i64,
}

#[post("/proxy")]
pub async fn proxy(settings: web::Form<Settings>) -> HttpResponse {
    match request_pzszerm(settings.into_inner()).await {
        Ok(data) => HttpResponse::Ok().body(data),
        Err(e) => e.to_response(),
    }
}

#[cached(size = 3, time = 86400, result = true)]
async fn request_pzszerm(settings: Settings) -> Result<String, Error> {
    let text = reqwest::get(format!(
        "https://pzszerm.pl/zawody/kalendarium-zawodow/?pg={}",
        settings.page
    ))
    .await?
    .text()
    .await?;
    let page = Html::parse_document(&text);
    Selector::parse("table")
        .ok()
        .and_then(|table_selector| page.select(&table_selector).next())
        .map(|table| table.html())
        .map(|html| format!("<div>{}</div>", html))
        .or_else(|| Some(text))
        .ok_or(Error::ScraperError)
}
