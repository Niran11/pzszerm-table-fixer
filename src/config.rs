use actix_cors::Cors;
use serde::Deserialize;
use std::fs;

#[derive(Deserialize)]
pub struct Config {
    pub port: i16,
    pub cors: CorsConfig,
}

#[derive(Clone, Deserialize)]
pub struct CorsConfig {
    pub allowed_origin: String,
    pub max_age: usize,
}

impl Config {
    pub fn new<'a>(path: &'a str) -> Result<Self, Box<dyn std::error::Error>> {
        let file_data = fs::read_to_string(path)?;
        Ok(serde_json::from_str(&file_data)?)
    }
}

impl CorsConfig {
    pub fn get_cors(&self) -> Cors {
        Cors::default()
            .max_age(Some(self.max_age))
            .allowed_origin(&self.allowed_origin)
            .allowed_methods(vec!["GET", "POST", "PUT", "DELETE", "PATCH"])
            .allowed_headers(vec!["Content-Type", "Cookies"])
            .expose_any_header()
            .supports_credentials()
    }
}
