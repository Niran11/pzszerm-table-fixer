use actix_files::Files;
use actix_web::{middleware::Logger, web, App, HttpServer};

mod config;
mod error;
mod proxy;

use config::Config;

#[actix_web::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let config_path = "config.json";
    let app_config = Config::new(config_path)?;
    let cors = app_config.cors.clone();
    env_logger::init();

    HttpServer::new(move || {
        App::new()
            .wrap(cors.get_cors())
            .wrap(Logger::default())
            .service(
                web::scope("/pzszerm")
                    .service(proxy::proxy)
                    .service(Files::new("/", "./static/").index_file("index.html")),
            )
            .service(proxy::proxy)
            .service(Files::new("/", "./static/").index_file("index.html"))
    })
    .bind(&format!("127.0.0.1:{}", app_config.port))?
    .run()
    .await?;
    Ok(())
}
