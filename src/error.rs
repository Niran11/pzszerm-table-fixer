use actix_web::{http::StatusCode, HttpResponse};

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("{1}")]
    ReqwasmError(StatusCode, String),

    #[error("Postprocessing scrapera się nie udał")]
    ScraperError,
}

impl Error {
    pub fn to_response(&self) -> HttpResponse {
        HttpResponse::build(self.status()).json(format!("{{\"error\":\"{}\"}}", self))
    }

    fn status(&self) -> StatusCode {
        match self {
            Self::ReqwasmError(status, _) => *status,
            Self::ScraperError => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}

impl From<reqwest::Error> for Error {
    fn from(err: reqwest::Error) -> Self {
        Self::ReqwasmError(
            err.status().unwrap_or(StatusCode::INTERNAL_SERVER_ERROR),
            err.to_string(),
        )
    }
}
