CREATE TABLE age_categories (
    id INTEGER NOT NULL PRIMARY KEY,
    code VARCHAR(4) NOT NULL UNIQUE
);

INSERT INTO age_categories
    (code) VALUES
    ('A6U8'),
    ('U10'),
    ('U12'),
    ('U14'),
    ('U17'),
    ('U20'),
    ('U23'),
    ('A24'),
    ('V');

CREATE TABLE pzszerm_age_categories_mapper (
    id INTEGER NOT NULL PRIMARY KEY,
    pzszerm_code VARCHAR(4) NOT NULL UNIQUE,
    category_id INTEGER NOT NULL UNIQUE,
    FOREIGN KEY (category_id)
        REFERENCES age_categories (id)
);

INSERT INTO pzszerm_age_categories_mapper 
    (pzszerm_code, category_id) VALUES
    ('SK', (SELECT id FROM age_categories WHERE code = 'A6U8')),
    ('Z', (SELECT id FROM age_categories WHERE code = 'U10')),
    ('D', (SELECT id FROM age_categories WHERE code = 'U12')),
    ('MŁ', (SELECT id FROM age_categories WHERE code = 'U14')),
    ('JMŁ', (SELECT id FROM age_categories WHERE code = 'U17')),
    ('J', (SELECT id FROM age_categories WHERE code = 'U20')),
    ('M', (SELECT id FROM age_categories WHERE code = 'U23')),
    ('S', (SELECT id FROM age_categories WHERE code = 'A24')),
    ('W', (SELECT id FROM age_categories WHERE code = 'V'));