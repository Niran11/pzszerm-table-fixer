CREATE TABLE weapons (
    id INTEGER NOT NULL PRIMARY KEY,
    code VARCHAR(5) NOT NULL UNIQUE
);

INSERT INTO weapons
    (code) VALUES
    ('EPEE'),
    ('FOIL'),
    ('SABRE');

CREATE TABLE pzszerm_weapons_mapper (
    id INTEGER NOT NULL PRIMARY KEY,
    pzszerm_code VARCHAR(4) NOT NULL UNIQUE,
    weapon_id INTEGER NOT NULL UNIQUE,
    FOREIGN KEY (weapon_id)
        REFERENCES weapons (id)
);

INSERT INTO pzszerm_weapons_mapper 
    (pzszerm_code, weapon_id) VALUES
    ('SZP', (SELECT id FROM weapons WHERE code = 'EPEE')),
    ('FL', (SELECT id FROM weapons WHERE code = 'FOIL')),
    ('SZB', (SELECT id FROM weapons WHERE code = 'SABRE'));
