CREATE TABLE pzszerm_tournaments (
    id INTEGER NOT NULL PRIMARY KEY,
    name TEXT NOT NULL,
    url TEXT NOT NULL,
    season_begin INTEGER NOT NULL,
    season_end INTEGER NOT NULL,
    duration_begin TEXT NOT NULL,
    duration_end TEXT NOT NULL,
    city_name TEXT NOT NULL
);

CREATE TABLE pzszerm_tournaments_age_categories (
    tournament_id INTEGER NOT NULL,
    category_id INTEGER NOT NULL,
    PRIMARY KEY (tournament_id, category_id),
    FOREIGN KEY (tournament_id) REFERENCES pzszerm_tournaments (id) ON DELETE CASCADE,
    FOREIGN KEY (category_id) REFERENCES age_categories (id)
);

CREATE TABLE pzszerm_tournaments_weapons (
    tournament_id INTEGER NOT NULL,
    weapon_id INTEGER NOT NULL,
    PRIMARY KEY (tournament_id, weapon_id),
    FOREIGN KEY (tournament_id) REFERENCES pzszerm_tournaments (id) ON DELETE CASCADE,
    FOREIGN KEY (weapon_id) REFERENCES weapons (id)
);
