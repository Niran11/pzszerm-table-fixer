use std::env;

use actix_files::Files;
use actix_web::{middleware::Logger, web, App, HttpServer};
use dotenvy::dotenv;
use fencfo_getter::create_getter_loop;
use sqlx::SqlitePool;

mod config;
mod error;
mod pzszerm;

use config::Config;

#[actix_web::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let config_path = "config.json";
    let app_config = Config::new(config_path)?;
    dotenv()?;
    let cors = app_config.cors.clone();
    let db_pool = SqlitePool::connect(&env::var("DATABASE_URL")?).await?;
    env_logger::init();
    create_getter_loop(db_pool.clone());

    HttpServer::new(move || {
        App::new()
            .wrap(cors.get_cors())
            .wrap(Logger::default())
            .app_data(web::Data::new(db_pool.clone()))
            .service(
                web::scope("/pzszerm")
                    .service(pzszerm::get)
                    .service(Files::new("/", "./static/").index_file("index.html")),
            )
            .service(pzszerm::get)
            .service(Files::new("/", "./static/").index_file("index.html"))
    })
    .bind(&format!("127.0.0.1:{}", app_config.port))?
    .run()
    .await?;
    Ok(())
}
