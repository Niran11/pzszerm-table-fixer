use actix_web::HttpResponse;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Database failure")]
    SqlxFailed,

    #[error("Could not load tournaments")]
    SerdeJsonFailed,
}

impl Error {
    pub fn to_response(&self) -> HttpResponse {
        HttpResponse::InternalServerError().json(format!("{{\"error\":\"{}\"}}", self))
    }
}

impl From<sqlx::Error> for Error {
    fn from(_: sqlx::Error) -> Self {
        Self::SqlxFailed
    }
}

impl From<serde_json::Error> for Error {
    fn from(_: serde_json::Error) -> Self {
        Self::SerdeJsonFailed
    }
}
