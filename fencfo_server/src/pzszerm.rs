use actix_web::{get, web, HttpResponse};
use cached::proc_macro::cached;
use cached::TimedCache;
use fencfo_pzszerm_store::PzszermTournament;
use sqlx::SqlitePool;

use crate::error::Error;

#[get("/get")]
pub async fn get(db_pool: web::Data<SqlitePool>) -> HttpResponse {
    match request_pzszerm(&db_pool).await {
        Ok(data) => HttpResponse::Ok().body(data),
        Err(e) => e.to_response(),
    }
}

#[cached(
    result = true,
    type = "TimedCache<bool, String>",
    create = "{ TimedCache::with_lifespan_and_capacity(3600, 1) }",
    convert = r#"{ db_pool.is_closed() }"#
)]
async fn request_pzszerm(db_pool: &SqlitePool) -> Result<String, Error> {
    let mut conn = db_pool.acquire().await?;
    Ok(serde_json::to_string(
        &PzszermTournament::get_ordered(&mut conn).await?,
    )?)
}
