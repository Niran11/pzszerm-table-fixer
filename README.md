# Pzszerm's page incoming tournaments table fixer

## abandoned in favor of fencfo
[fencfo](https://gitlab.com/Niran11/fencfo)

## What's pzszerm
It's Polish Fencing Federation.

## Why?
Back in the day pzszerm had nice incoming tournaments table -
tournaments closest to now were shown on top of the page,
tournaments further into future were below it.
Then there was separate webpage for old tournaments.
<br/>
Then someone decided it's gonna be good idea to write page anew. It was good idea.
Problem is that whoever wrote that page merged both pages.
And because of that now we had tournament that's furtherst into the future at the top and current tournaments somewhere in the second page or so.
Then on next pages there are old tournaments.
This way instead of `opening page -> checking first tournament, because it's closest to now -> leaving page` now you have to
`open page -> search for current month -> possibly go to next page -> search for current month -> search for tournament closest to now`.
Process became way longer, way more uncomfortable.
<br/>
This application loads all future tournaments available on pzszerm page and shows them in old, proper order.

## Other features
When writing this application I discovered that filtering by age and weapon was broken too.
Filtering by age was showing only old tournaments and by weapon wasn't filtering at all!
<br/>
So I implemented filtering in js - it just filters loaded table's data on webpage instead of calling server.
<br/>
Also - pzszerm's table is exactly that - a `<table>` html tag. Problem with that is on mobile you can't see half of table's data.
This application uses bunch of `div`s to make it all responsive.

## Installation / Usage
Application is just bunch of static files, tailwind css styles and a server to serve all that in rust.
So all you need binaries wise is rust language, `npm` and `cargo run`.<br/>
But before running you have to copy `config.json.template` rename it to `config.json`
and change config to your liking.
after running you can access application at eg. `http://localhost:port/pzszerm`.

## How it works
There's bunch of `fencfo_*` rust crates - they are responsible for loading, parsing and storing informations about tournaments available in pzszerm page.
They run in endless loop once a day to update to possibly newer versions of tournaments.
Then we have `fencfo_server` which starts loop defined in `fencfo_getter` (our endless parsing/storing loop) and starts webserver to serve our webpage.
Our frontend is static `index.html` and [tables.js](static/tables.js) file which is responsible for calling our server for tournaments data.
Our server returns all future tournaments in database in ascending order (so - "oldest" tournaments first, most future ones last).
All it has to do is to display it however it wants.
<br/>
In general parsing pzszerm page is about finding all `<tr>`s in `<table>` (only table on this page)
and trying to match it to be month and year (`td` with `colspan=5` having `<strong>` inside it),
tournaments subtable's header (bunch of `th`s with `.tab-top` having `<strong>` inside)
or tournament data (`td`s width `.tab-row`).
Then it parses all that into useful data for application (ignoring date and headers, turning rows into tournament with eg. name, weapons, age categories, etc.).
After that that parsed data is saved into sqlite database for frontend to load.
<br/>
When filtering - iterates over all tournaments, checks if some weapon/age category is selected
and matching, if not then removes it from the copied list.
Then displays it the same way as onload function

## Performance
Before having endless loop backend was acting as proxy which on request from frontend requested page from pzszerm
and sent it back to frontend (all this to avoid CORS problems). Even with caching it was slow, nobody checks tournaments thousand times a day or something.
<br/>
After adding sqlite database with hour long cache on retrieval there shouldn't be any performance problems.
It just reads data from drive or loads copy of it from memory if cached
instead of requesting different webpage while being in the middle of request.
